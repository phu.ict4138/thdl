import express from "express";
import cors from "cors";
import { Sequelize, DataTypes, QueryTypes, Op } from "sequelize";

const sequelize = new Sequelize("thdl", 'root', null, {
	host: "localhost",
	dialect: "mysql",
	logging: false,
});

const connectToDatabase = async () => {
	try {
		await sequelize.authenticate();
		console.log("Connect to database success!");
	} catch (error) {
		console.log(error);
	}
}

await connectToDatabase();

const app = express();

app.use(express.json());
app.use(express.urlencoded({
	extended: true,
	limit: "50mb",
}));
app.use(cors());

const Category = sequelize.define("category", {
	categoryId: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false
	},
	name: {
		type: DataTypes.STRING,
	},
}, {
	freezeTableName: "true"
});

const Book = sequelize.define("book", {
	bookId: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false
	},
	url: {
		type: DataTypes.STRING,
	},
	name: {
		type: DataTypes.STRING,
	},
	author: {
		type: DataTypes.STRING,
	},
	categoryId: {
		type: DataTypes.INTEGER,
	},
	image: {
		type: DataTypes.STRING,
	},
	description: {
		type: DataTypes.TEXT,
	}
}, {
	freezeTableName: true
});

Book.belongsTo(Category, {
	foreignKey: "categoryId",
	as: "category"
});
Category.hasMany(Book, {
	foreignKey: "categoryId",
	as: "books"
})

// await sequelize.sync({
// 	force: false,
// 	alter: true
// });

console.log(123123);

app.post("/api/v1/category/create", async (req, res) => {
	const { name } = req.body;
	if (!name) {
		return res.status(400).json({
			result: "fail",
			message: "fail"
		});
	}
	const category = await Category.create({
		name: name
	});
	return res.status(200).json({
		result: "success",
		category
	});
});

app.get("/api/v1/category", async (req, res) => {
	const categories = await Category.findAll();
	return res.status(200).json({
		result: "success",
		categories
	});
})

app.post("/api/v1/book/create", async (req, res) => {
	const { name, categoryId, url, author, description, image } = req.body;
	const book = await Book.create({
		name, categoryId, url, author, description, image
	});
	return res.status(200).json({
		result: "success",
		book
	});
});

app.get("/api/v1/book", async (req, res) => {
	const page = typeof (req.query.page) == "undefined" ? 0 : Number(req.query.page);
	const limit = typeof (req.query.limit) == "undefined" ? 20 : Number(req.query.limit);
	const query = req.query.query ?? "";
	const categoryId = Number(req.query.categoryId ?? 0);
	const where = {
	}
	if (categoryId != 0) {
		where["categoryId"] = categoryId
	}
	// const _where = JSON.parse(JSON.stringify(where))
	if (query) {
		where["name"] = {
			[Op.like]: `%${query}%`
		}
	}
	const { count, rows: books } = await Book.findAndCountAll({
		limit: limit,
		offset: limit * page,
		where: {
			...where,
		},
		include: {
			model: Category,
			as: "category"
		},
		attributes: {
			exclude: ["categoryId", "description"]
		}
	});
	return res.status(200).json({
		result: "success",
		page,
		limit,
		total: count,
		books
	});
})

app.get("/", async (req, res) => {
	return res.send("<h1>Hello from server</h1>");
});

app.listen(4000, () => {
	console.log("App is running at port", 4000);
});
