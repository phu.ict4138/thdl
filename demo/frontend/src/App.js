import './App.css';
import { useEffect, useState } from "react";
import Tippy from "@tippyjs/react/headless";
import { useSpring, animated } from "@react-spring/web";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleDoubleLeft, faAngleDoubleRight, faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons"

const PostItem = ({ name, image, author, category, url }) => {

    return (
        <div
            className='flex flex-col px-2 w-1/4 py-2 cursor-pointer hover:-translate-y-1'
            onClick={() => {
                const a = document.createElement("a");
                a.href = url;
                a.target = "__blank";
                a.click();
                a.remove();
            }}
        >
            <img
                alt={name}
                src={image}
                className='object-cover h-[450px]' />
            <div className='line-clamp-2 '>{name}</div>
            <div className='line-clamp-1'>{"Danh mục: " + category?.name}</div>
            <div className='line-clamp-1'>{"Tác giả: " + author}</div>
        </div>
    );
}

const className = {
    wrapper: `flex mt-10 mb-10`,
    button: `
        h-10 w-10 mx-1 bg-gray-100 rounded flex items-center justify-center
        text-gray-700 text-sm cursor-pointer font-semibold border border-gray-200
        hover:bg-black hover:text-white
    `,
    buttonActive: `kkk 
        h-10 w-10 mx-1 bg-black rounded flex items-center justify-center
        text-white text-sm cursor-pointer font-semibold
    `,
}

const Pagination = ({
    total,
    page,
    setPage
}) => {

    return (
        <div className={className.wrapper}>
            <div
                onClick={() => setPage(1)}
                className={className.button}>
                <FontAwesomeIcon
                    icon={faAngleDoubleLeft}
                />
            </div>
            <div
                onClick={() => setPage(Math.max(1, page - 1))}
                className={className.button}>
                <FontAwesomeIcon
                    icon={faAngleLeft}
                />
            </div>
            {(page > total - 2 && page > 3) &&
                <div className={className.button}>
                    {"..."}
                </div>}
            {(page == total && total > 2) &&
                <div
                    onClick={() => setPage(page - 2)}
                    className={className.button}>
                    {page - 2}
                </div>
            }
            {(page - 1 > 0) &&
                <div
                    onClick={() => setPage(page - 1)}
                    className={className.button}>
                    {page - 1}
                </div>
            }
            <div className={className.buttonActive}>
                {page}
            </div>
            {(page < total) &&
                <div
                    onClick={() => setPage(page + 1)}
                    className={className.button}>
                    {page + 1}
                </div>
            }
            {(page == 1 && total > 2) &&
                <div
                    onClick={() => setPage(page + 2)}
                    className={className.button}>
                    {page + 2}
                </div>
            }

            {(page < total - 1 && total > 3) &&
                <div className={className.button}>
                    {"..."}
                </div>}
            <div
                onClick={() => setPage(Math.min((page + 1), total))}
                className={className.button}>
                <FontAwesomeIcon
                    icon={faAngleRight}
                />
            </div>
            <div
                onClick={() => setPage(total)}
                className={className.button}>
                <FontAwesomeIcon
                    icon={faAngleDoubleRight}
                />
            </div>
        </div>
    )
}




const DropDown = ({ component, render, placement = "bottom-end", transformOrigin = "top" }) => {

    const [show, setShow] = useState(false);
    const config = { tension: 500 };
    const initialStyles = { transform: "scaleY(0)" };
    const [styles, spring] = useSpring(() => initialStyles);

    const onMount = () => {
        spring.start({
            transform: "scaleY(1)",
            config
        });
    }

    const onHide = ({ unmount }) => {
        spring.start({
            transform: "scaleY(0)",
            onRest: unmount,
            config: { ...config, clamp: true }
        })
    }

    return (
        <div>
            <Tippy
                interactive={true}
                visible={show}
                placement={placement}
                onClickOutside={() => setShow(false)}
                animation={true}
                onMount={onMount}
                onHide={onHide}
                render={(attrs) => (
                    <animated.div
                        style={{
                            ...styles,
                            transformOrigin: transformOrigin
                        }}
                        onClick={() => setShow(!show)}
                        {...attrs}>
                        {render}
                    </animated.div>
                )}>
                <div onClick={() => setShow(!show)}>
                    {component}
                </div>
            </Tippy>
        </div>
    )
}

const Header = ({ value, onChange, categories, currentCategory, onChangeCategory, onKeyDown }) => {

    return (
        <div className='w-[80rem] h-16 mb-5 mt-5 flex flex-row items-center'>
            <div className='ml-4 h-10 flex w-56 border rounded border-gray-400 bg-red-100 overflow-hidden'>
                <input
                    placeholder='Tìm kiếm'
                    value={value}
                    onKeyDown={onKeyDown}
                    onChange={onChange}
                    className='flex-1 outline-none px-1' />
            </div>
            <div className='ml-4 mr-4'>Danh mục:</div>
            <DropDown
                render={
                    <div className='w-[300px] h-[200px] bg-white shadow-md overflow-y-scroll rounded'>
                        {[{ categoryId: 0, name: "Tất cả" }].concat(categories).map((item, index) => (
                            <div
                                key={item.categoryId ?? index}
                                onClick={() => onChangeCategory(item)}
                                className='h-8 flex items-center hover:bg-gray-300 pl-2 cursor-pointer'>
                                {item.name}
                            </div>
                        ))}
                    </div>
                }
                component={
                    <div className='h-10 rounded border bg-gray-100 border-gray-400 pl-2 w-[300px] flex items-center cursor-pointer'>{currentCategory.name}</div>
                } />
        </div>
    )
}

function App() {
    const [data, setData] = useState({
        total: 0,
        books: []
    });

    const [categories, setCategories] = useState([]);
    const [currentCategory, setCurrentCategory] = useState({
        categoryId: 0,
        name: "Tất cả"
    });

    const [page, setPage] = useState(1);

    const [value, setValue] = useState("");

    const getData = async (_page) => {
        console.log(JSON.stringify({
            page, value
        }));
        const result = await fetch(`http://localhost:4000/api/v1/book?query=${value}&categoryId=${currentCategory.categoryId}&page=${_page ?? page - 1}`, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(res => res.json());
        console.log(result);
        if (result.result == "success") {
            setData({
                total: result.total,
                books: result.books
            })
        }
    }

    const getCategory = async () => {
        const result = await fetch(`http://localhost:4000/api/v1/category`, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(res => res.json());
        console.log(result);
        if (result.result == "success") {
            setCategories(result.categories)
        }
    }

    useEffect(() => {
        // setPage(0);
        getData();
        getCategory();
    }, [currentCategory.categoryId, page])

    return (
        <div className="w-full flex flex-col items-center">
            <Header
                categories={categories}
                value={value}
                onKeyDown={e => {
                    if (e.key == "Enter") {
                        setPage(1);
                        getData(0);
                    }
                }}
                onChange={e => setValue(e.target.value)}
                onChangeCategory={e => {
                    setPage(1);
                    setCurrentCategory(e)
                }}
                currentCategory={currentCategory} />
            <div className='flex flex-row w-[80rem] min-h-screen flex-wrap'>
                {data.books.map((item, index) => (
                    <PostItem
                        key={item.id ?? index}
                        {...item} />
                ))}
            </div>
            <Pagination
                total={Math.floor(data.total / 20)}
                setPage={setPage}
                page={page} />
        </div>
    );
}

export default App;
