const wait = (ms) => new Promise(e => setTimeout(e, ms));
let block = document.querySelector('.jeg_block_loadmore ')
while (block) {
    let a = block.querySelector('a');
    if (a.className == "disabled") break;
    a.click()
    await wait(1000);
    block = document.querySelector('.jeg_block_loadmore ');
}
let h2 = document.querySelector(".jeg_heroblock_wrapper").querySelectorAll("h2");
let h3 = document.querySelector(".jeg_posts_wrap").querySelectorAll("h3");
const x = [];
for (const i of h2) {
    x.push(i.querySelector("a").href)
}
for (const i of h3) {
    x.push(i.querySelector("a").href)
}
const data = x.join("\n");
console.log(data);