from NhaSachMienPhi import NhaSachMienPhiSpider
from apscheduler.schedulers.twisted import TwistedScheduler
from scrapy.crawler import CrawlerProcess
import time

process = CrawlerProcess()
def crawl():
    process.crawl(NhaSachMienPhiSpider)

scheduler = TwistedScheduler()
scheduler.add_job(crawl, 'interval', seconds=30*1)
scheduler.start(paused=False)
process.start(False)
