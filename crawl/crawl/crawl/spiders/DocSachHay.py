import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.http.response.html import HtmlResponse
import time
import json
from datetime import datetime, date
from bs4 import BeautifulSoup
import requests
from util import process_string


class DocSachHaySpider(scrapy.Spider):
    def __init__(self, name="DocSachHay-spider", **kwargs):
        super(DocSachHaySpider, self).__init__(name, **kwargs)
        self.base_url = "https://docsachhay.net/"
        self.data = []
    

    def start_requests(self):
        for i in range(1, 196):
        # for i in range(1, 5):
            yield scrapy.Request(
                    url=f"https://docsachhay.net/cat/all?page={i}",
                    callback=self.parse_page,
                )

    def parse_page(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        list_block = html.find_all("li", class_="novel-item")
        list_url = [i.find("a").get("href") for i in list_block]
        for url in list_url:
            yield scrapy.Request(
                url=url,
                callback=self.parse_data,
            )
        
    def parse_data(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        name = ""
        name_element = html.find("h1", class_="novel-title text2row")
        if name_element:
            name = name_element.get_text()
        author = ""
        author_block = html.find("div", class_="author")
        if author_block:
            author = author_block.get_text().split(":")[1].strip()
        category = ""
        category_block = html.find("div", class_="categories")
        if category_block:
            category = category_block.find("li").get_text().strip()
        file = ""
        file_block = html.find("a", class_="button pdf")
        if file_block:
            file = file_block.get("href")
        content = ""
        content_block = html.find("div", class_="content c-editor")
        if content_block:
            content = content + process_string(content_block.get_text())
        rate = ""
        rate_block = html.find("div", class_="your-rating")
        if rate_block:
            rate = rate_block.find("span").get_text()
        image = ""
        image_block = html.find("div", class_="fixed-img")
        if image_block:
            image = image_block.find("img").get("data-src")
        information_block = html.find("div", class_="header-stats")
        chapter = ""
        view = "" 
        if information_block:
            chapter = information_block.find_all("span")[0].find("strong").get_text().strip()
            view = information_block.find_all("span")[1].find("strong").get_text().strip()
        self.data.append({
            "url": response.url,
            "name": name,
            "rate": rate,
            "author": author,
            "chapter": chapter,
            "view": view,
            "category": category,
            "image": image,
            "file": file,
            "description": content
        })

    
    def closed(self, reason):
        data = {
            "url": [],
            "name": [],
            "rate": [],
            "author": [],
            "chapter": [],
            "view": [],
            "category": [],
            "image": [],
            "file": [],
            "description": []
        }
        for i in self.data:
            data["url"].append(i["url"])    
            data["name"].append(i["name"])    
            data["rate"].append(i["rate"])    
            data["author"].append(i["author"]) 
            data["chapter"].append(i["chapter"]) 
            data["view"].append(i["view"]) 
            data["category"].append(i["category"]) 
            data["image"].append(i["image"]) 
            data["file"].append(i["file"]) 
            data["description"].append(i["description"]) 
        import pandas as pd
        df = pd.DataFrame(data)
        df.to_excel("./data_docsachhay.xlsx", index=False, sheet_name="data")
            
        

def main() -> None:
    process = CrawlerProcess()
    process.crawl(DocSachHaySpider)
    process.start()

if __name__ == "__main__":
    main()

