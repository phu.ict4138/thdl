import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.http.response.html import HtmlResponse
import time
import json
from datetime import datetime, date
from bs4 import BeautifulSoup
import requests
from util import process_string


class ThuVienSachSpider(scrapy.Spider):
    def __init__(self, name="ThuVienSach-spider", **kwargs):
        super(ThuVienSachSpider, self).__init__(name, **kwargs)
        self.base_url = "https://thuviensach.vn"
        self.data = []
    

    def start_requests(self):
        for i in range(1, 181):
        # for i in range(1, 2):
            yield scrapy.Request(
                    url=f"https://thuviensach.vn/thu-vien/page/{i}",
                    callback=self.parse_page,
                )

    def parse_page(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        list_block = html.find_all("h3")
        list_url = [i.find("a").get("href") for i in list_block[:-2]]
        for url in list_url:
            yield scrapy.Request(
                url=self.base_url + url,
                callback=self.parse_data,
            )
        
    def parse_data(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        name = ""
        name_element = html.find("h1", class_="fs20")
        if name_element is not None:
            name = name_element.get_text()
        information_block = html.find("div", class_="col-xs-12 pr0 pl0")
        information = {}
        category = ""
        if information_block:
            infor_list = information_block.find_all("p", class_="mt10 mb10")
            category = infor_list[-1].find("a").get_text()
            for i in infor_list[:-1]:
                key, value = i.get_text().split(":")[:2]
                information[key.strip()] = value.strip()
        content = ""
        content_block = html.find("div", class_="col-xs-12 col-md-8")
        if content_block:
            p_list = content_block.find_all("p")
            p_list = [i for i in p_list if len(i.attrs) == 0][:-2]
            for i in p_list:
                content = content + process_string(i.get_text()) + " "
        image = ""
        author = information.get("Tác giả", "")
        page = information.get("Số trang", "")
        view = information.get("Lượt xem/nghe", "")
        read = information.get("Lượt đọc", "")
        download = information.get("Lượt tải", "")
        image_block = html.find("div", class_="col-xs-12 pd0 size-shop_catalog")
        if image_block:
            image = image_block.find("img").get("src")
        self.data.append({
            "url": response.url,
            "name": name,
            "author": author,
            "category": category,
            "page": page,
            "view": view,
            "read": read,
            "download": download,
            "image": image,
            "description": content
        })

    
    def closed(self, reason):
        data = {
            "url": [],
            "name": [],
            "author": [],
            "category": [],
            "page": [],
            "view": [],
            "read": [],
            "download": [],
            "image": [],
            "description": []
        }
        for i in self.data:
            data["url"].append(i["url"])    
            data["name"].append(i["name"])    
            data["author"].append(i["author"]) 
            data["category"].append(i["category"]) 
            data["page"].append(i["page"]) 
            data["view"].append(i["view"]) 
            data["read"].append(i["read"]) 
            data["download"].append(i["download"]) 
            data["image"].append(i["image"]) 
            data["description"].append(i["description"]) 
        import pandas as pd
        df = pd.DataFrame(data)
        df.to_excel("./data/data_thuviensach.xlsx", index=False, sheet_name="data")
            
        

def main() -> None:
    process = CrawlerProcess()
    process.crawl(ThuVienSachSpider)
    process.start()

if __name__ == "__main__":
    main()

