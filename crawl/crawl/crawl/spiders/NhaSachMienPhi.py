import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.http.response.html import HtmlResponse
import time
import json
from datetime import datetime, date
from bs4 import BeautifulSoup
import requests
from util import process_string


class NhaSachMienPhiSpider(scrapy.Spider):
    def __init__(self, name="NhaSachMienPhi-spider", **kwargs):
        super(NhaSachMienPhiSpider, self).__init__(name, **kwargs)
        self.base_url = "https://nhasachmienphi.com/"
        self.data = []
    

    def start_requests(self):
        for i in range(1, 220):
            yield scrapy.Request(
                    url=f"https://nhasachmienphi.com/tat-ca-sach/page/{i}",
                    callback=self.parse_page,
                )

    def parse_page(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        list_block = html.find_all("h4", class_="mg-t-10")
        list_url = [i.find("a").get("href") for i in list_block]
        for url in list_url:
            yield scrapy.Request(
                url=url,
                callback=self.parse_data,
            )
        
    def parse_data(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        name = ""
        name_element = html.find("h1", class_="tblue fs-20")
        if name_element is not None:
            name = name_element.get_text()
        information_block = html.find("div", class_="col-xs-12 col-sm-8 col-md-8 col-lg-8")
        author = ""
        if information_block is not None:
            author = information_block.find_all("div", "mg-t-10")[1].get_text().split(":")[1].strip()
        category = ""
        category_block = information_block.find("div", "mg-tb-10")
        if category_block:
            category = category_block.get_text().split(":")[1].strip()
        file = ""
        file_block = html.find("a", class_="button pdf")
        if file_block:
            file = file_block.get("href")
        content = ""
        content_block = html.find("div", class_="content_p content_p_al")
        if content_block:
            p_list = content_block.find_all("p")
            for i in p_list:
                content = content + process_string(i.get_text()) + " "
            if file == "":
                file_block = content_block.find("a")
                if file_block:
                    file = file_block.get("href")
        image = ""
        image_block = html.find("div", class_="col-xs-12 col-sm-4 col-md-4 col-lg-4")
        if image_block:
            image = image_block.find("img").get("src")
        self.data.append({
            "url": response.url,
            "name": name,
            "author": author,
            "category": category,
            "image": image,
            "file": file,
            "description": content
        })

    
    def closed(self, reason):
        data = {
            "url": [],
            "name": [],
            "author": [],
            "category": [],
            "image": [],
            "file": [],
            "description": []
        }
        for i in self.data:
            data["url"].append(i["url"])    
            data["name"].append(i["name"])    
            data["author"].append(i["author"]) 
            data["category"].append(i["category"]) 
            data["image"].append(i["image"]) 
            data["file"].append(i["file"]) 
            data["description"].append(i["description"]) 
        import pandas as pd
        df = pd.DataFrame(data)
        df.to_excel("./data_nhasachmienphi.xlsx", index=False, sheet_name="data")
            
        

def main() -> None:
    process = CrawlerProcess()
    process.crawl(NhaSachMienPhiSpider)
    process.start()

if __name__ == "__main__":
    main()

