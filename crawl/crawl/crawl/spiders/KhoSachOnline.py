import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.http.response.html import HtmlResponse
import time
import json
from datetime import datetime, date
from bs4 import BeautifulSoup
import requests
from util import process_string
import random

user_agents = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:88.0) Gecko/20100101 Firefox/88.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Edge/92.0.902.55',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Edge/91.0.864.59',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36 Edg/92.0.902.55',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.59',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36 Edg/92.0.902.55',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 Edg/91.0.864.59',
    'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36 Edg/92.0.902.55',
]


class KhoSachMienPhiSpider(scrapy.Spider):

    custom_settings = {
        'RETRY_TIMES': 0,
    }

    def __init__(self, name="KhoSachMienPhi-spider", **kwargs):
        super(KhoSachMienPhiSpider, self).__init__(name, **kwargs)
        self.base_url = "https://khosachonline.com"
        self.data = []
        self.urls_list = []
    

    def start_requests(self):
        # for i in range(1, 632):
        for i in range(1, 632):
            if i < 20:
                time.sleep(0.2)
                yield scrapy.Request(
                        url=f"https://khosachonline.com/sach?page={i}",
                        callback=self.parse_page,
                    )
            else:
                time.sleep(0.1)
                yield scrapy.Request(
                        url=f"https://khosachonline.com/sach?page={i}",
                        callback=self.parse_page,
                    )

    def parse_page(self, response: HtmlResponse):
        # print(f"Existing settings: {self.settings.attributes.keys()}")
        # print(self.settings.attributes)
        html = BeautifulSoup(response.text, "lxml")
        list_block = html.find_all("div", class_="entry-title")
        list_url = [i.find("a").get("href") for i in list_block]
        for url in list_url:
            self.urls_list.append(self.base_url + url)
        # for url in list_url:
        #     time.sleep(0.4)
        #     yield scrapy.Request(
        #         url=self.base_url + url,
        #         callback=self.parse_data,
        #     )
        
    def parse_data(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        name = ""
        name_element = html.find("h1")
        if name_element is not None:
            name = name_element.get_text()
        information_block = html.find("ul", class_="iconlist")
        author = ""
        category = ""
        if information_block is not None:
            author = information_block.find_all("li")[1].get_text().split(":")[1].strip()
            category = information_block.find_all("li")[0].get_text().split(":")[1].strip()
        file = ""
        file_block = html.find("a", class_="button pdf")
        if file_block:
            file = file_block.get("href")
        content = ""
        content_block = html.find("div", class_="tab-container")
        if content_block:
            content += process_string(content_block.get_text())
        image = ""
        image_block = html.find("div", class_="flexslider")
        if image_block:
            image = image_block.find("img").get("data-src")
        self.data.append({
            "url": response.url,
            "name": name,
            "author": author,
            "category": category,
            "image": image,
            "file": file,
            "description": content
        })

    
    def closed(self, reason):
        data = {
            "url": [],
            "name": [],
            "author": [],
            "category": [],
            "image": [],
            "file": [],
            "description": []
        }
        for i in self.data:
            data["url"].append(i["url"])    
            data["name"].append(i["name"])    
            data["author"].append(i["author"]) 
            data["category"].append(i["category"]) 
            data["image"].append(i["image"]) 
            data["file"].append(i["file"]) 
            data["description"].append(i["description"]) 
        import pandas as pd
        df = pd.DataFrame(data)
        df.to_excel("./data/data_khosachonline.xlsx", index=False, sheet_name="data")
        # df2 = pd.DataFrame(data={"url": self.urls_list})
        # df2.to_excel("./urls.xlsx", index=False, sheet_name="data")
            
        

def main() -> None:
    process = CrawlerProcess()
    process.crawl(KhoSachMienPhiSpider)
    process.start()

if __name__ == "__main__":
    main()

