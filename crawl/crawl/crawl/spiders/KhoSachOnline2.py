import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.http.response.html import HtmlResponse
import time
import json
from datetime import datetime, date
from bs4 import BeautifulSoup
import requests
from util import process_string
import random


class KhoSachMienPhiSpider(scrapy.Spider):

    custom_settings = {
        'RETRY_TIMES': 0,
        "DOWNLOAD_DELAY": 0.1
    }

    def __init__(self, name="KhoSachMienPhi-spider", **kwargs):
        super(KhoSachMienPhiSpider, self).__init__(name, **kwargs)
        self.base_url = "https://khosachonline.com"
        self.data = []
        self.urls_list = []
    

    def start_requests(self):
        import pandas as pd
        df = pd.read_excel("./urls.xlsx", sheet_name="data")
        list_url = df["url"].tolist()
        n = len(list_url)
        for i, url in enumerate(list_url):
            print(f"[{i}/{n}] - [{round(i * 100 /n, 2)}%]")
            yield scrapy.Request(
                    url=url,
                    callback=self.parse_data,
                )


    def parse_page(self, response: HtmlResponse):
        # print(f"Existing settings: {self.settings.attributes.keys()}")
        # print(self.settings.attributes)
        html = BeautifulSoup(response.text, "lxml")
        list_block = html.find_all("div", class_="entry-title")
        list_url = [i.find("a").get("href") for i in list_block]
        for url in list_url:
            self.urls_list.append(self.base_url + url)
        # for url in list_url:
        #     time.sleep(0.4)
        #     yield scrapy.Request(
        #         url=self.base_url + url,
        #         callback=self.parse_data,
        #     )
        
    def parse_data(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        name = ""
        name_element = html.find("h1")
        if name_element is not None:
            name = name_element.get_text()
        information_block = html.find("ul", class_="iconlist")
        author = ""
        category = ""
        information = {}
        if information_block is not None:
            li = information_block.find_all("li")
            for j in li:
                key, value = j.get_text().split(":")[:2]
                information[key.strip()] = value.strip()
        file = ""
        file_block = html.find("a", class_="button pdf")
        if file_block:
            file = file_block.get("href")
        content = ""
        content_block = html.find("div", class_="tab-container")
        if content_block:
            content += process_string(content_block.get_text())
        image = ""
        image_block = html.find("div", class_="flexslider")
        if image_block:
            image = image_block.find("img").get("data-src")
        self.data.append({
            "url": response.url,
            "name": name,
            "author": author,
            "category": category,
            "information": information,
            "image": image,
            "file": file,
            "description": content
        })

    
    def closed(self, reason):
        data = {
            "url": [],
            "name": [],
            "author": [],
            "category": [],
            "image": [],
            "description": []
        }
        for i in self.data:
            data["url"].append(i["url"])    
            data["name"].append(i["name"])    
            data["author"].append(i["information"].get("Tác giả", "")) 
            data["category"].append(i["information"].get("Thể loại", "")) 
            data["image"].append(i["image"]) 
            data["description"].append(i["description"]) 
        import pandas as pd
        df = pd.DataFrame(data)
        df.to_excel("./data/data_khosachonline.xlsx", index=False, sheet_name="data")
        # df2 = pd.DataFrame(data={"url": self.urls_list})
        # df2.to_excel("./urls.xlsx", index=False, sheet_name="data")
            
        

def main() -> None:
    process = CrawlerProcess()
    process.crawl(KhoSachMienPhiSpider)
    process.start()

if __name__ == "__main__":
    main()

