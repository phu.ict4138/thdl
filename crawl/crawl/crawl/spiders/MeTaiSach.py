import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.http.response.html import HtmlResponse
import time
import json
from datetime import datetime, date
from bs4 import BeautifulSoup
import requests
from util import process_string
import random


class MeTaiSachSpider(scrapy.Spider):

    def __init__(self, name="MeTaiSach-spider", **kwargs):
        super(MeTaiSachSpider, self).__init__(name, **kwargs)
        self.base_url = "https://khosachonline.com"
        self.data = []
        self.urls_list = []
    

    def start_requests(self):
        f = open("./metaisachlink.txt", "r", encoding="utf-8")
        a = f.readlines()
        list_url = [i.strip() for i in a]
        n = len(list_url)
        for i, url in enumerate(list_url):
            print(f"[{i}/{n}] - [{round(i * 100 /n, 2)}%]")
            yield scrapy.Request(
                    url=url,
                    callback=self.parse_data,
                )
        
    def parse_data(self, response: HtmlResponse):
        html = BeautifulSoup(response.text, "lxml")
        name = ""
        name_element = html.find("h1", class_="jeg_post_title")
        if name_element is not None:
            name = name_element.get_text()
        author = ""
        category = ""
        category_block = html.find("div", class_="ebook-category")
        if category_block:
            category = category_block.get_text().split(":")[1].strip()
        author_block = html.find("div", class_="ebook-author")
        if author_block:
            author_text = author_block.get_text().strip()
            if len(author_text) > 5:
                author = author_text.split(":")[1].strip()
        content = ""
        content_block = html.find("div", class_="ebook-content")
        if content_block:
            p_list = content_block.find_all("p")
            for i in p_list:
                content = content + i.get_text() + " "
        if content == "":
            content_block = html.find("div", class_="entry-content")
            if content_block:
                p_list = content_block.find_all("p")
                for i in p_list:
                    content = content + i.get_text() + " "
        if content == "":
            content_block = html.find("div", class_="wps-tab-text  wps-active")
            if content_block:
                p_list = content_block.find_all("p")
                for i in p_list:
                    content = content + i.get_text() + " "
        if content == "":
            content_block = html.find("div", class_="the_content_wrapper")
            if content_block:
                p_list = content_block.find_all("div")
                for i in p_list:
                    content = content + i.get_text() + " "
        if content == "":
            content_block = html.find("div", class_="ebook-content")
            if content_block:
                content = process_string(content_block.get_text())
        image = ""
        image_block = html.find("div", class_="col-xs-12 col-sm-12 col-md-4 col-lg-4 thumnail-ebook")
        if image_block:
            image_ = image_block.find("img")
            if image_:
                image = image_.get("src")
        self.data.append({
            "url": response.url,
            "name": name,
            "author": author,
            "category": category,
            "image": image,
            "description": content
        })

    
    def closed(self, reason):
        data = {
            "url": [],
            "name": [],
            "author": [],
            "category": [],
            "image": [],
            "description": []
        }
        for i in self.data:
            data["url"].append(i["url"])    
            data["name"].append(i["name"])    
            data["author"].append(i["author"]) 
            data["category"].append(i["category"]) 
            data["image"].append(i["image"]) 
            data["description"].append(i["description"]) 
        import pandas as pd
        df = pd.DataFrame(data)
        df.to_excel("./data/data_metaisach.xlsx", index=False, sheet_name="data")
        # df2 = pd.DataFrame(data={"url": self.urls_list})
        # df2.to_excel("./urls.xlsx", index=False, sheet_name="data")
            
        

def main() -> None:
    process = CrawlerProcess()
    process.crawl(MeTaiSachSpider)
    process.start()

if __name__ == "__main__":
    main()

