import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QStackedWidget
from PyQt5.uic import loadUi
import pickle

dict_reverse = {
    0: 'Truyện Ngắn - Ngôn Tình',
    1: 'Đam Mỹ - Cường Công Thụ Sủng',
    2: 'Tâm Lý - Kỹ Năng Sống',
    3: 'Tiểu Thuyết',
    4: 'Trinh Thám - Hình Sự',
    5: 'Cổ Tích - Thần Thoại - Cổ Đại',
    6: 'Văn Học Việt Nam',
    7: 'Kiếm Hiệp - Tiên Hiệp',
    8: 'Kinh Tế - Quản Lý',
    9: 'Ma - Kinh Dị - Linh Dị',
    10: 'Đô Thị - Quan Trường',
    11: 'Y Học - Sức Khỏe',
    12: 'Marketing - Bán hàng',
    13: 'Lịch Sử - Chính Trị',
    14: 'Truyên Teen - Tuổi Học Trò',
    15: 'Triết Học',
    16: 'Huyền bí - Giả Tưởng - Huyền Huyễn',
    17: 'Hồi Ký - Tuỳ Bút',
    18: 'Điền Văn',
    19: 'Hiện Đại',
    20: 'Võng Du',
    21: 'Truyện Cười - Tiếu Lâm',
    22: 'Bách Hợp',
    23: 'Xuyên Không',
    24: 'Trọng Sinh',
    25: 'Khoa Học - Kỹ Thuật',
    26: 'Văn Hóa - Nghệ Thuật',
    27: 'Tử Vi - Phong Thủy',
    28: 'Sắc',
    29: 'Phiêu Lưu - Mạo Hiểm',
    30: 'Ngược',
    31: 'Sủng',
    32: 'Tài Liệu Học Tập',
    33: 'Khoa Huyễn',
    34: 'Cường Thủ Hào Đoạt',
    35: 'Quân Sự',
    36: 'Học Ngoại Ngữ',
    37: 'Thơ',
    38: 'Truyện Tranh',
    39: 'Ẩm thực - Nấu ăn',
    40: 'Thể Thao - Nghệ Thuật',
    41: 'Nông - Lâm - Ngư',
    42: 'Nữ Cường',
    43: 'Kiến Trúc - Xây Dựng',
    44: 'Mạt Thế',
    45: 'Cung Đấu',
    46: 'Nữ Phụ',
    47: 'Lịch Sử',
    48: 'Tôn Giáo - Tâm Linh',
    49: 'Thư Viện Pháp Luật',
    50: 'Công Nghệ Thông Tin',
    51: 'Dị Giới',
    52: 'Dị Năng',
    53: 'Gia Đấu',
    54: 'Gián Điệp - Phản Gián',
    55: 'Trộm Mộ'
 }

def process_string(s: str):
    if not s:
        return ""
    s = s.replace("\xa0", "")
    s = s.replace("“", "")
    s = s.replace("”", "")
    s = s.replace("(", " ")
    s = s.replace("...", " ")
    s = s.replace(";", " ")
    s = s.replace(")", " ")
    s = s.replace("!", " ")
    s = s.replace("?", " ")
    s = s.replace("-", " ")
    s = s.replace("–", " ")
    s = s.replace("【", " ")
    s = s.replace("】", " ")
    s = s.replace("—", " ")
    s = s.replace("《", " ")
    s = s.replace("》", " ")
    s = s.replace("┃", " ")
    s = s.replace("*", " ")
    s = s.replace("\"", " ")
    s = s.replace("…", " ")
    s = s.replace(":", " ")
    s = s.replace("_", " ")
    s = s.replace("[", " ")
    s = s.replace("]", " ")
    s = s.replace("’", " ")
    while "  " in s:
        s = s.replace("  ", " ")
    if s.startswith(" "):
        s = s[1:]
    if s.endswith(" "):
        s = s[:-1]
    return s.lower()

class App(QMainWindow):
    def __init__(self):
        super(App, self).__init__()
        loadUi("app.ui", self)
        self.predict_button.clicked.connect(self.predict)
        self.load_model()

    def load_model(self):
        with open('tf_idf.pk', 'rb') as fp:
            tf_idf = pickle.load(fp)
        with open('svd.p', 'rb') as fp:
            svd = pickle.load(fp)
        with open('bayes.p', 'rb') as fp:
            bayes = pickle.load(fp)
        with open('linear.p', 'rb') as fp:
            linear = pickle.load(fp)
        with open('random.p', 'rb') as fp:
            random = pickle.load(fp)
        with open('svm.p', 'rb') as fp:
            svm = pickle.load(fp)
        self.tf_idf = tf_idf
        self.svd = svd
        self.bayes = bayes
        self.linear = linear
        self.random = random
        self.svm = svm

    def predict(self):
        name = self.name.toPlainText()
        author = self.author.toPlainText()
        category = self.category.toPlainText()
        description = self.description.toPlainText()
        print(name)
        print(author)
        print(category)
        print(description)
        _name = process_string(name)
        _category = process_string(category)
        _author = process_string(author)
        _description = process_string(description)
        f = [_name] * 2 + [_author] * 1 + [_category] * 6 + [_description]
        sent = " ".join(f)
        sen_ = self.tf_idf.transform([sent])
        sen_ = self.svd.transform(sen_)
        random = self.random.predict(sen_).tolist()[0]
        logic = self.linear.predict(sen_).tolist()[0]
        bayes = self.bayes.predict(sen_).tolist()[0]
        svm = self.svm.predict(sen_).tolist()[0]
        d = {i: 0 for i in range(56)}
        d[bayes] +=  1.0
        d[random] += 1.001
        d[logic] += 1.01
        d[svm] += 1.1
        index = 0
        for i in d:
            if d[i] > d[index]:
                index = i
        self.random_.setText(dict_reverse[random])
        self.logic_.setText(dict_reverse[logic])
        self.bayes_.setText(dict_reverse[bayes])
        self.svm_.setText(dict_reverse[svm])
        self.vote.setText(dict_reverse[index])



if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setApplicationDisplayName("Một con vịt xòe ra hai con thằn lằn con")
    widget = QStackedWidget()
    main = App()
    widget.addWidget(main)
    widget.setFixedWidth(1700)
    widget.setFixedHeight(900)
    widget.show()
    try:
        sys.exit(app.exec_())
    except:
        print("Closing")